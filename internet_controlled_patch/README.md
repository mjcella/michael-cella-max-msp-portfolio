This code allows you to control a max patch from anywhere with an internet connection.

Steps to run:

1. If you don't have NPM installed, install NPM.
2. Run `npm install` in the root directory where this README.md file exists.
3. Open internet_controlled_patch.maxpat; turn audio on inside Max.
4. Click the "script start" message; wait a few seconds while the node script initializes and runs.
5. Click the message box next to the "open this URL" comment. That will open a new tab in your browser, to a publicly accessible page on the internet.
6. In the UI of the webpage, hold your mouse down and create a two-dimenstional "gesture". On mouseup, that XY data will get sent into this patch.
7. The data from the browser could be mapped onto anything (it's Max/MSP after all!). For example, a friend of mine "played" my modular synth from thousands of miles away. In this patch, X controls frequency and Y controls amplitude of a sine wave.

Author: Michael Cella, 2018
