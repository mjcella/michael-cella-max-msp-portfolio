/*
control max from anywhere
author: Michael Cella, 2018
*/

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const hbs = require('handlebars');
const fs = require('fs');
const maxAPI = require('max-api');
const localtunnel = require('localtunnel');
const ngrok = require('ngrok');

let url;

app.use(bodyParser.urlencoded({ extended: false, limit: '50mb',parameterLimit: 1000000 }));

maxAPI.addHandler("port", (msg) => {
	// set up localtunnel on the port selected by the user
	let ngrok_port = parseInt(msg);
	(async function() {
		url = await ngrok.connect(ngrok_port);
		maxAPI.post(url);
		maxAPI.post(`publicly availabel URL created at ${url}, port ${ngrok_port}`);
		maxAPI.outlet(`ngrok \nmax launchbrowser ${url}`);
	})();
	app.listen(ngrok_port);
});

maxAPI.addHandler("stoptunnel", (msg) => {
	maxAPI.post(`stoptunnel message received`);;
	ngrok.disconnect();
	maxAPI.post(`ngrok closed at ${url}`);
});

app.get('/', (req, res) => {
	// render the UI
	fs.readFile('./templates/gesture_ui.html', 'utf8', (err, data) => {
		if (err) throw err;
		let template = hbs.compile(data);
		let html = template(data);
		res.send(html);
	})
});

app.post('/data', (req, res) => {
	maxAPI.outlet('clear');

	// get the unique MS timestamps from the gesture
	let gesture = req.body;
	let all_keys = Object.keys(gesture);
	let filtered_keys = [];
	for (let i = 0; i < all_keys.length; i++ ) {
		let key = all_keys[i].split('[')[0];
		filtered_keys.push(key);
	}
	filtered_keys = filtered_keys.filter((item, pos) => {
		return filtered_keys.indexOf(item) == pos;
	});

	// run a hook on gesture init
	maxAPI.outlet('on');

	// send the x and y values for each timestamp into max. they get "piped"
	// in max to be sent out by the corresponding delay amount in MS
	for (let i = 0; i < filtered_keys.length; i++ ) {
		let x_val = gesture[filtered_keys[i] + '[x]'];
		let y_val = gesture[filtered_keys[i] + '[y]'];
		let delay_ms = parseInt(filtered_keys[i]);
		maxAPI.outlet(`${delay_ms} ${x_val} ${y_val}`);
		if ( i == (filtered_keys.length - 1 ) ) {
			// run a hook upon gesture completion, for e.g. returning oscillator amplitude to 0
			maxAPI.outlet('off');
		}
	}
	res.send('OK');
});
